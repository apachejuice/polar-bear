# polar-bear

An open-source remote development protocol and reference implementation

Name inspired by the [Polar bear bonjour meme](https://knowyourmeme.com/memes/bonjour-bear), much like the bear comes of of a tunnel this bear also uses a tunnel. A tunnel between your computer and the remote server.
